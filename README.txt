*********************************************************************
* soapUI toolz 2012
* author: robert.nemet@gmail.com
*********************************************************************

soapUI tools are set of tools that should help you in daily soapUI usage.

To run utils from it you need to add it to {soapui.home}/ext folder, while
dependencies need to be in lib folder ( {soapui.home}/../lib ).

Dependencies ( see build.gradle file for versions ), which goes in lib folder: 
* ant-javamail, ant, antlr, ant-launcher