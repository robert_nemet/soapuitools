/*
 * stoolz, Copyright (c) 2012 devnull.com
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 *
 * You may not use this work except in compliance with the
 * Licence.
 *
 * You may obtain a copy of the Licence at:
 *
 *  http://ec.europa.eu/idabc/eupl
 *
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
package stoolz.utils

class SendMail {

	def ant = new AntBuilder()

	// must
	def fromAddress
	def toAddress
	def mailHost
	def mailPort
	def user
	def password
	def ssl = 'false'
	def enableStartTLS = 'false'

	def send(toAddress, subject, mailMessage, attachmentDir, filePattern) {

		ant.mail(mailhost:mailHost, mailport: mailPort, messagemimetype:'text/html', subject: subject) {
			from(address:fromAddress)
			to(address:toAddress)
			message(mailMessage)
			attachments(){
				fileset(dir:attachmentDir){ include(name:'**/$filePattern') }
			}
		}

	}

	def send(toAddress, subject, mailMessage) {

		ant.mail(mailhost:mailHost, mailport: mailPort, messagemimetype:'text/html', subject: subject, ssl: ssl, enableStartTLS: enableStartTLS, user:user, password:password) {
			from(address:fromAddress)
			to(address:toAddress)
			message(mailMessage)
		}
	}

	def enableSSL() {
		this.ssl = 'true'
	}

	def disableSSL() {
		this.ssl = 'false'
	}

	def configure(mailHost, mailPort, fromAddress, user, password, ssl, enableStartTLS) {

		this.mailHost = mailHost
		this.mailPort = mailPort
		this.fromAddress = fromAddress
		this.user = user
		this.password = password
		this.ssl = ssl
		this.enableStartTLS = enableStartTLS

	}
	
	def initGMail(username, password) {
		this.user = username
		this.fromAddress = username
		this.password = password
		mailHost = "smtp.googlemail.com"
		mailPort = "587"
		ssl = 'no'
		enableStartTLS = 'on'
	}

}
