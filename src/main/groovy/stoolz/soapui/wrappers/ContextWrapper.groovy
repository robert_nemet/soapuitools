/*
 * stoolz, Copyright (c) 2012 devnull.com
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 *
 * You may not use this work except in compliance with the
 * Licence.
 *
 * You may obtain a copy of the Licence at:
 *
 *  http://ec.europa.eu/idabc/eupl
 *
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
package stoolz.soapui.wrappers

import com.eviware.soapui.model.ModelItem
import com.eviware.soapui.model.support.AbstractSubmitContext
import com.eviware.soapui.model.support.ModelSupport
import com.eviware.soapui.model.testsuite.TestSuite

class ContextWrapper {

	def AbstractSubmitContext context;

	public ContextWrapper(AbstractSubmitContext context) {
		this.context = context
	}

	def getProject() {
		ModelSupport.getModelItemProject(context.getModelItem())
	}

	/*
	 * Try pass missing method to context 
	 */
	def methodMissing(String name, args) {
		def method
		context.metaClass.methods.find {
			if ( it.name == name )
				method = it
		}
		if( method.isVargsMethod() )
			context."$name"(args)
		else
			context."$name"()
	}

	def getTestSuite() {
		def modelItem = context?.getModelItem()

		if( modelItem )
			while( !( modelItem instanceof TestSuite ) && modelItem != null ) {
				modelItem = modelItem.getParent()
			}

		return modelItem
	}
	
	def getTestCase() {
		ModelSupport.getModelItemTestCase(context.getModelItem())
	}
	
	def getWorkspace() {
		getProject()?.getWorkspace()
	}
	
}
