/*
 * stoolz, Copyright (c) 2012 devnull.com
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 *
 * You may not use this work except in compliance with the
 * Licence.
 *
 * You may obtain a copy of the Licence at:
 *
 *  http://ec.europa.eu/idabc/eupl
 *
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
package stoolz.soapui.wrappers

import com.eviware.soapui.impl.wsdl.testcase.WsdlTestRunContext
import com.eviware.soapui.model.support.ModelSupport;

class RunContextWrapper extends ContextWrapper {

	public RunContextWrapper(context) {
		super( context )
	}
	
	def propertyMissing(String name) {
		def property = context?.getProperty(name)
		if ( property )
			return property.value.toString()
			
		for( testStep in context?.testCase.getTestStepList() ) 
			if( testStep.name == name )
				return testStep
			
		def project = ModelSupport.getModelItemProject( context.getModelItem() )			
		return project.getTestSuiteByName(name)
	}
	
	public void setProperty( String name, value ) {
		context.&setProperty( name, value)
	}
	
}
