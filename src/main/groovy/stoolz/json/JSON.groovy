/*
 * stoolz, Copyright (c) 2012 devnull.com
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 *
 * You may not use this work except in compliance with the
 * Licence.
 *
 * You may obtain a copy of the Licence at:
 *
 *  http://ec.europa.eu/idabc/eupl
 *
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
package stoolz.json

import groovy.json.JsonBuilder

class JSON extends LinkedHashMap{
	
	def searchMap = [:]
	def searchIndex = [:]

	public JSON(Map jsonMap) {
		super(jsonMap)
	}

	@Override
	public String toString() {
		new JsonBuilder(this).toPrettyString()
	}

	def findFirst(String name) {
		if ( !searchMap."$name" )
			element( this, name )	
		if( searchMap."$name" ) {
			searchIndex.put name, 0
			return searchMap."$name"[0]
		} else
			return null
	}

	def element(Map map, name) {
		for( key in map.keySet() ) {
			if( key == name ) 
				addToSearchMap(name, map."$name")
			else {
				def nextElement = map.get(key)
				if( nextElement instanceof List || nextElement instanceof Map ) {
					def result = element( nextElement, name)
					if ( result ) 
						addToSearchMap(name, result)
				}
			}
		}
	}

	private addToSearchMap(name, result) {
		def tokenValues = searchMap."$name" == null ? [] : searchMap."$name"
		tokenValues.add result
		searchMap.put name, tokenValues
	}

	def element(List list, name) {
		for( item in list) 
			element( item, name)
	}
	
	def findNext(name) {
		findNext(name, false)
	}	
	
	def findNext(String name, Boolean wrap) {
		if( searchMap."$name" ) {
			def next = searchIndex."$name" + 1
			if( searchMap."$name".size > next ) {
				searchIndex.put name, next
				return searchMap."$name"[next]
			} else if ( wrap ) {
					searchIndex.put name, 0
					return searchMap."$name"[0]
				} else {
					return searchMap."$name"[searchIndex."$name"]
				}
		} else
			return findFirst(name)
	}
}
