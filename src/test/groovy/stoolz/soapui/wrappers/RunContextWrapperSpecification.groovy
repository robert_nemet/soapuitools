/*
 * stoolz, Copyright (c) 2012 devnull.com
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 *
 * You may not use this work except in compliance with the
 * Licence.
 *
 * You may obtain a copy of the Licence at:
 *
 *  http://ec.europa.eu/idabc/eupl
 *
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
package stoolz.soapui.wrappers

import com.eviware.soapui.impl.wsdl.testcase.WsdlTestRunContext
import com.eviware.soapui.model.project.ProjectFactoryRegistry
import com.eviware.soapui.model.support.ModelSupport
import com.eviware.soapui.support.UISupport

import spock.lang.Specification

import static org.hamcrest.MatcherAssert.assertThat
import static org.hamcrest.Matchers.*

class RunContextWrapperSpecification extends Specification {

	def context
	def soapUIContext

	def setup() {
		def path = this.getClass().getResource("/soapui projects/MyTestApp2-soapui-project.xml").toURI().toURL()
		def project = ProjectFactoryRegistry.getProjectFactory("wsdl").createNew(path.toString())
		def testSuite = project.getTestSuiteByName("TestSuite - Groovy")
		def testStep = testSuite.getTestCaseByName("DS").getTestStepByName("Groovy Script")
		soapUIContext = new WsdlTestRunContext(testStep)
		context = new RunContextWrapper(soapUIContext)
	}

	def "For given context get running test case"() {
		expect:
		assertThat context.testCase, is( soapUIContext.testCase )
	}

	def "For given context get owning test suite"() {
		expect:
		assertThat context.testSuite, is( soapUIContext.testCase.testSuite )
	}

	def "For given context get project"() {
		expect:
		assertThat context.project, is( soapUIContext.testCase.testSuite.project )
	}

	def "Access existing property value"() {
		when:
		soapUIContext.setProperty("test property", "test property value")

		then:
		assertThat context."test property", is( soapUIContext.getProperty("test property") )
	}
	
	def "Set existing property value with out using setValue method"() {
		when:
		soapUIContext.setProperty("test property", "XXX")
		context."test property" = "YYY"
		
		then:
		assertThat context."test property", is( soapUIContext.getProperty("test property") )
	}
	
	def "Set new property on context wrapper"() {
		when:
		context."test property" = "YYY"
		
		then:
		assertThat context."test property", is( soapUIContext.getProperty("test property") )
	}

	def "Access test step of context's test case as property"() {
		expect:
		assertThat context."DataSource", is( soapUIContext.testCase.getTestStepByName("DataSource") )
	}
	
	def "Access any test suite as property"() {
		expect:
		assertThat context."General Test", is( ModelSupport.getModelItemProject(soapUIContext.getModelItem()).getTestSuiteByName("General Test"))
	}
}
