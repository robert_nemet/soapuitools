/*
 * stoolz, Copyright (c) 2012 devnull.com
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 *
 * You may not use this work except in compliance with the
 * Licence.
 *
 * You may obtain a copy of the Licence at:
 *
 *  http://ec.europa.eu/idabc/eupl
 *
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
package stoolz.json

import spock.lang.Specification

import static org.hamcrest.MatcherAssert.assertThat
import static org.hamcrest.Matchers.*

import stoolz.json.util.JsonUtil


class JsonSpecification extends Specification {
	
	def "Locate item with just a name"() {
		when: "Have a simple JSON"
		def json = JsonUtil.parse( this.getClass().getResourceAsStream("/simple.json").text )

		then:
		assertThat json.findFirst('id'), is("file") 
	}
	
	def "Locate item going down with structure"() {
		when: "Have a simple JSON"
		def json = JsonUtil.parse( this.getClass().getResourceAsStream("/simple.json").text )

		then:
		assertThat json.menu.id, is ("file")
		assertThat json.findFirst('id'), is("file") 
	}
	
	def "Search for non existing element"() {
		when: "Have a simple JSON"
		def json = JsonUtil.parse( this.getClass().getResourceAsStream("/simple.json").text )
		
		then: "Search for non existing element and get null"
		nullValue json.findFirst('test')
		nullValue json.findNext('test')
	}
	
	def "Search for next elements"() {
		when: "Have a simple JSON"
		def json = JsonUtil.parse( this.getClass().getResourceAsStream("/simple.json").text )
		
		then: "Next and next to the bottom of document"
		assertThat json.findNext("value"), is("New")
		assertThat json.findNext("value"), is("Open")
		assertThat json.findNext("value"), is("Close")
		assertThat json.findNext("value"), is("File")
		assertThat json.findNext("value"), is("File")
		
		and: "Wrapped search: when come to the bottom of document go on the top"
		assertThat json.findFirst("value"), is("New")
		assertThat json.findNext("value"), is("Open")
		assertThat json.findNext("value"), is("Close")
		assertThat json.findNext("value", true), is("File")
		assertThat json.findNext("value", true), is("New")
		assertThat json.findNext("value"), is("Open")
		assertThat json.findNext("value"), is("Close")
		
		
	}

}
